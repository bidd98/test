$(document).ready(function () {
    var $imageContainer = $('#image-container');
    $('#btn-show').click(function(){
        $.ajax({
            type: 'GET',
            url: 'http://localhost:11406/images',
            data: {},
            success: function(response) {
                // console.log(response);
                html = '';
                response.forEach(function(item){
                    html += '<img style="width:200px;height:auto; margin: 15px;" src="'+item.path+'"/>';
                });
                $imageContainer.html(html);
            }
        });

    });
});
