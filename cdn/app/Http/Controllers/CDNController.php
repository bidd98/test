<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CDNController extends Controller
{
    /**
     * @var request
     * @return mixed
     */
    public function upload(Request $request)
    {
        $data = $request->all();
        $file = $data['image'];

        $name = Storage::disk('resources_image')->put('', $file);

        $message = 'upload successfully, filename: ' . $name;

        Image::create([
            'path' => getenv('APP_URL'). '/images/' .$name,
        ]);

        return view('welcome', compact('message'));
        
    }
}
