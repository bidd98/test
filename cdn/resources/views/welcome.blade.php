<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif
        <!--  method="post" action="/cdn/upload" -->
        <div>
            <form method="post" action="/cdn/upload" enctype="multipart/form-data">

                <div class="form-group">
                    @csrf
                    <label for="exampleFormControlFile1">Upload Image</label>
                    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                    <button id="btn-add" class="btn btn-primary mt-2" type="submit">Submit</button>
                    @isset($message)
                    <p style="color: green">{{$message}}</p>
                    @endif
                </div>
            </form>
            <button type="button" id="btn-show" class="btn btn-primary">Show Image</button>
            <div class="form-group" id="image-container">

            </div>
        </div>
    </div>
</body>

</html>
