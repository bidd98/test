<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        return Image::all();
    }
}
