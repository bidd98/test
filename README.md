# Laravel Test

Tasks I did to finish this test:

1. Init Docker Containers:
 + Time: 60p

2. Setup Postgres DB:
 + Time: 15p

3. Setup CDN Laraver/Nodejs:
 + Time: 30p

4. Setup Laravel API:
 + Time: 10p

# Please follow below instructions to test this app:
1. Go to 'test' folder and start Docker containers:<br />
`docker-compose up`

2. Composer install laravel cdn and laravel api: <br />
`docker-compose run --r app_cdn composer install` <br />
`docker-compose run --r app_api composer install` <br />

3. Setup file .env <br />
`cd cdn && cp .env.example .env` <br />

```properties
APP_URL=http://localhost:11401

DB_CONNECTION=pgsql
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=test_db
DB_USERNAME=bidd
DB_PASSWORD=root
```

Then copy this file to laravel api source: <br />
`cp .env ../app/.env` <br />
Then edit this line: <br />
`APP_URL=http://localhost:11406`

4. Grant permission for cdn and api: <br />
`docker-compose run --r app_cdn chmod -R 777 storage/` <br />
`docker-compose run --r app_api chmod -R 777 storage/` <br />

5. Generate Encryption Key: <br />
`docker-compose run --r app_cdn php artisan key:generate` <br />
`docker-compose run --r app_cdn php artisan key:generate` <br />

6. Migrate table for storing Image Paths: <br />
`docker-compose run --r app_cdn php artisan migrate` <br />

7. Install node and npm on Laravel CDN <br />
`docker-compose run --r node npm install` <br />

8. Set Node to watch if someone uploads image to cdn <br />
`docker-compose run --r node npm run watch` <br />

[VPS Production] (http://35.240.226.141:11401)



